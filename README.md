# Tanuki Inc

> Showcase for Ops features.

## How to use

1. Navigate to the [demo environment](https://gitlab-org-monitor-tanuki-inc.34.69.64.147.nip.io/)
2. Select 'Generate Error'.
    - This will send an error to the connected Sentry server.
3. Select 'View Errors' to view the errors in GitLab.

Errors may take up to a minute to appear in GitLab/Sentry.

If you need to access the Sentry project, it is located on the GitLab Sentry instance: https://sentry.gitlab.net/gitlab/tanuki-inc/.

Further details and discussion about this project may be found on the original issue: https://gitlab.com/gitlab-org/gitlab/issues/33666.

## Access

Anyone may view and interact with the environment and GitLab project.

To view the error tracking page, you must have the role of Reporter or above.

## Development

Sentry integration is only switched on when the app is in production mode - a developer environment running in dev mode will not be able to send errors to sentry.

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

Made with [Nuxt.js](https://nuxtjs.org).
